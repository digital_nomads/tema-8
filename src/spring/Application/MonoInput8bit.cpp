#include<spring\Application\MonoInput8bit.h>
#include<iostream>
#include <qendian.h>

MonoInput8bit::MonoInput8bit(const unsigned int ac_dSampleRate)
{
	mAudioFormat.setSampleRate(ac_dSampleRate);
	mAudioFormat.setChannelCount(1);
	mAudioFormat.setSampleSize(16);
	mAudioFormat.setSampleType(QAudioFormat::UnSignedInt);
	mAudioFormat.setByteOrder(QAudioFormat::LittleEndian);
	mAudioFormat.setCodec("audio/pcm");
}

MonoInput8bit::~MonoInput8bit()
{

}

qint64 MonoInput8bit::readData(char* data, qint64 maxlen)
{
	Q_UNUSED(data);
	Q_UNUSED(maxlen);
	return -1;
}
qint64 MonoInput8bit::writeData(const char *data, qint64 len)
{
	/*
	for (qint64 i = 0; i < len;i++) {
	std::cout << data[i];
	}
	*/

	const auto *ptr = reinterpret_cast<const unsigned char*>(data);
	mSamples.clear();
	mSamples.resize(len / 2);

	for (qint64 i = 0; i < len / 2; i++) {
		double value = qFromLittleEndian<qint64>(ptr);
		mSamples[i] = value;
		ptr += 2;
	}

	return len;
}

QAudioFormat MonoInput8bit::getAudioFormat()
{
	return mAudioFormat;
}

QVector<double> MonoInput8bit::vecGetData()
{
	return mSamples;
}
