#include<spring\Application\MonoInput32BigEndian.h>
#include<iostream>
#include <qendian.h>

MonoInput32BigEndian::MonoInput32BigEndian(const unsigned int ac_dSampleRate)
{
	mAudioFormat.setSampleRate(ac_dSampleRate);
	mAudioFormat.setChannelCount(1);
	mAudioFormat.setSampleSize(32);
	mAudioFormat.setSampleType(QAudioFormat::SignedInt);
	mAudioFormat.setByteOrder(QAudioFormat::BigEndian);
	mAudioFormat.setCodec("audio/pcm");
}

MonoInput32BigEndian::~MonoInput32BigEndian()
{

}

qint64 MonoInput32BigEndian::readData(char* data, qint64 maxlen)
{
	Q_UNUSED(data);
	Q_UNUSED(maxlen);
	return -1;
}
qint64 MonoInput32BigEndian::writeData(const char *data, qint64 len)
{
	/*
	for (qint64 i = 0; i < len;i++) {
	std::cout << data[i];
	}
	*/

	const auto *ptr = reinterpret_cast<const unsigned char*>(data);
	mSamples.clear();
	mSamples.resize(len / 2);

	for (qint64 i = 0; i < len / 2; i++) {
		double value = qFromLittleEndian<qint64>(ptr);
		mSamples[i] = value;
		ptr += 2;
	}

	return len;
}

QAudioFormat MonoInput32BigEndian::getAudioFormat()
{
	return mAudioFormat;
}

QVector<double> MonoInput32BigEndian::vecGetData()
{
	return mSamples;
}
