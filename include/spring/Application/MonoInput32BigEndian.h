#include<qiodevice.h>
#include <qaudioformat.h>
#include <qvector.h>

class MonoInput32BigEndian :public QIODevice
{
public:
	MonoInput32BigEndian(const unsigned int ac_dSampleRate);
	~MonoInput32BigEndian();

	qint64 readData(char* data, qint64 maxlen);
	qint64 writeData(const char *data, qint64 len);

	QAudioFormat getAudioFormat();
	QVector<double> vecGetData();

private:
	QAudioFormat mAudioFormat;
	QVector<double> mSamples;
};

