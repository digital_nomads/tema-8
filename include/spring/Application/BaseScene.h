#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcustomplot.h>
#include <spring\Application\MonoInput.h>
#include <spring\Application\MonoInput8bit.h>
#include <spring\Application\MonoInput32BigEndian.h>
#include <QAudioInput.h>



namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();

      void mp_InitPlotters();

  public slots:
      void mp_BackButton();
      void mp_PlotRandom();
      void mp_PlotClean();

      void mf_StartTimer();
      void mf_StopTimer();
      
	
  private:
     double mv_dRefreshRate;
		QWidget * centralWidget;
      QCustomPlot* mv_customPlot;
      QCustomPlot* mv_customPlotFreq;

      QTimer mv_Timer;
      QAudioInput* mAudioInput;
      //MonoInput* mMonoInput;
	  MonoInput8bit* mMonoInput;
	  //MonoInput32BigEndian* mMonoInput;

	};
}
